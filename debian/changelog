sphinx-issuetracker (0.11-3) UNRELEASED; urgency=medium

  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Tue, 13 Feb 2018 09:59:19 +0100

sphinx-issuetracker (0.11-2) unstable; urgency=medium

  * Team upload.
  * Add a patch to set refdomain for all refnodes (closes: #876932).
  * Drop the transitional package. The new name is available since Wheezy.
  * Update debian/copyright.
  * Bump Standards-Version to 4.1.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 03 Oct 2017 22:46:22 +0700

sphinx-issuetracker (0.11-1) unstable; urgency=low

  * Team upload.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Dmitry Shachnev ]
  * New upstream release.
    + Build-depend on python-requests.
    + Rebase all patches.
    + Update install and docs files.
  * Use dh_python2 instead of dh_pysupport (closes: #745383).
    + Build-depend on dh-python instead of python-support.
    + Use X-Python-Version instead of XS-Python-Version.
  * Bump debhelper compatibility level to 9.
    + Update debhelper build-dependency.
    + Drop dh_compress override, not needed with new debhelper.
  * Bump Standards-Version to 3.9.5, no changes needed.
  * Update Homepage URL, the previous one is no longer working.
  * Delete egg-info directory in clean target (closes: #671196).

  [ Michael Fladischer ]
  * Add lintian override for missing upstream tarball PGP signature.
  * Drop versioned dependency on python-all as there are no prior
    versions in the archive.
  * Use correct format URL for d/copyright.

 -- Dmitry Shachnev <mitya57@gmail.com>  Tue, 26 Aug 2014 20:57:18 +0400

sphinx-issuetracker (0.9-1) unstable; urgency=low

  * New upstream release.
  * Update handle_missing_packages.patch.
  * Refresh do_not_build_license.patch and move_css.patch for new
    offsets.
  * Install CHANGES.rst as upstream changelog.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Thu, 29 Sep 2011 13:31:15 +0200

sphinx-issuetracker (0.8-1) unstable; urgency=low

  * New upstream version.
  * Upload to unstable.
  * Change binary package name to python-sphinxcontrib.issuetracker.
  * Fix object.inv location for python-sphinx.
  * Recommend python-debianbts as patch has been included upstream.
  * Move python-launchpadlib to Recommends as it is optional.
  * Add patch to conditionally add intersphinx mappings, depending on the
    presence of files.
  * Add patch to disable issue tracker integration during build to prevent
    network access.
  * Add patch to gracefully handle exceptions caused by missing
    packages.
  * Bump sphinx dependency to (>= 1.0.7+dfsg-1~)
  * Bumped Standards-Version to 3.9.2 (no change necessary).
  * Clean up debian/* using wrap-and-sort.
  * Use dh_sphinxdoc.
  * Mention JIRA as supported issue tracker.
  * Add patch to disable inclusion of license text in sphinx build.
  * Remove python-lxml from Recommends as it is no longer used.
  * Use consistent wording of 'issue tracker'.
  * Build documentation in dh_auto_build.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Sun, 28 Aug 2011 18:20:48 +0200

sphinx-issuetracker (0.5.4-1) experimental; urgency=low

  * Initial release. (Closes: #603136)

 -- Fladischer Michael <FladischerMichael@fladi.at>  Thu, 11 Nov 2010 10:24:45 +0100
